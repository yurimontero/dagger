package es.yurimontero.dagger.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import es.yurimontero.dagger.vo.OOF

@Dao
interface OofDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(oof: OOF)

    @Query("SELECT * FROM OOF")
    fun getOof(): LiveData<List<OOF>>


}