package es.yurimontero.dagger.ui

import android.support.v7.util.DiffUtil
import android.view.View
import com.bumptech.glide.Glide
import es.yurimontero.dagger.R
import es.yurimontero.dagger.ui.common.DataBoundListAdapter
import es.yurimontero.dagger.utils.AppExecutors
import es.yurimontero.dagger.vo.User
import kotlinx.android.synthetic.main.item_user.view.*
import javax.inject.Inject
import javax.inject.Singleton

var userClickCallback: ((User) -> Unit)? = null

@Singleton
class UserListAdapter @Inject constructor(
    appExecutors: AppExecutors
    // private val userClickCallback: ((User) -> Unit)?
) : DataBoundListAdapter<User>(
    appExecutors = appExecutors,
    layoutRes = R.layout.item_user,
    diffCallback = object : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.firstName == newItem.firstName
                    && oldItem.lastName == newItem.lastName
        }
    }
) {


    override fun bind(binding: View, item: User) {
        binding.setOnClickListener { userClickCallback?.invoke(item) }
        binding.txtFistName.text = item.firstName
        binding.txtLastName.text = item.lastName
        Glide.with(binding.context)
            .load("https://lorempixel.com/64/64/sports/${item.id}")
            .into(binding.imgUser)
    }

    fun setCallback(callback: (User) -> Unit) {
        userClickCallback = callback
    }

}
