package es.yurimontero.dagger.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import es.yurimontero.dagger.vo.User

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(users: List<User>)

    @Query("SELECT * FROM user")
    fun getUsers(): LiveData<List<User>>

    @Delete
    fun delete(user: User)

}
