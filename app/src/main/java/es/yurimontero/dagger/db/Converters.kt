package es.yurimontero.dagger.db

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import es.yurimontero.dagger.vo.Product

class Converters {

    @TypeConverter
    fun fromString(value: String): Product {
        val objectType = object : TypeToken<Product>() {}.type
        return Gson().fromJson(value, objectType)
    }

    @TypeConverter
    fun fromProduct(product: Product): String {
        return Gson().toJson(product)
    }

}
