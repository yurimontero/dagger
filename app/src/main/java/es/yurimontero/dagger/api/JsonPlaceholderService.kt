package es.yurimontero.dagger.api

import android.arch.lifecycle.LiveData
import es.yurimontero.dagger.vo.User
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Path

interface JsonPlaceholderService {

    @GET("users")
    fun getUsers(): LiveData<ApiResponse<List<User>>>

    @DELETE("users/{id}")
    fun deleteUser(@Path("id") id: Int)

}