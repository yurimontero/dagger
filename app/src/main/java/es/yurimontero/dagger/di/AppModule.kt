package es.yurimontero.dagger.di

import android.app.Application
import android.arch.persistence.room.Room
import dagger.Module
import dagger.Provides
import es.yurimontero.dagger.api.JsonPlaceholderService
import es.yurimontero.dagger.db.DB
import es.yurimontero.dagger.db.OofDao
import es.yurimontero.dagger.db.UserDao
import es.yurimontero.dagger.utils.AppExecutors
import es.yurimontero.dagger.utils.LiveDataCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module(
    includes = [
        ViewModelModule::class,
        ActivityModule::class]
)
class AppModule {

    @Singleton
    @Provides
    fun provideJsonPlaceholderService(): JsonPlaceholderService {
        return Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
            .create(JsonPlaceholderService::class.java)
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): DB {
        return Room
            .databaseBuilder(app, DB::class.java, "DB.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideUserDao(db: DB): UserDao {
        return db.userDao()
    }

    @Singleton
    @Provides
    fun provideOofDao(db: DB): OofDao {
        return db.oofDao()
    }

    @Singleton
    @Provides
    fun provideAppExecutors(): AppExecutors {
        return AppExecutors(
            Executors.newFixedThreadPool(3),
            Executors.newSingleThreadExecutor(),
            AppExecutors.MainThreadExecutor()
        )
    }

}
