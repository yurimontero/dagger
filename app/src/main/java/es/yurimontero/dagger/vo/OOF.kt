package es.yurimontero.dagger.vo

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class OOF(@PrimaryKey(autoGenerate = true) val id: Int, val name: String, val product: Product)

data class Product(val ean: String, val type: String)