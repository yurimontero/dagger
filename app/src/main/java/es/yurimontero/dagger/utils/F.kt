package es.yurimontero.dagger.utils

import android.app.Activity
import android.content.Context
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import timber.log.Timber

fun Activity?.log(message: String) {
    this?.apply {

        val tag = Thread.currentThread().stackTrace[3]?.run {
            className.substring(className.lastIndexOf(".") + 1)
                .split("$")
                .filter {
                    when (it.toIntOrNull()) {
                        is Int -> false
                        else -> true
                    }
                }.joinToString(".")
        } ?: this.javaClass.simpleName

        Timber.tag(tag).d(message)
    }
}

fun Context.inflate(@LayoutRes layout: Int): View {
    return LayoutInflater.from(this).inflate(layout, LinearLayout(this), false)
}
