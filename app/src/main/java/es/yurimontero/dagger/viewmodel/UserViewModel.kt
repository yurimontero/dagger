package es.yurimontero.dagger.viewmodel

import android.arch.lifecycle.ViewModel
import es.yurimontero.dagger.repository.UserRepository
import es.yurimontero.dagger.vo.OOF
import es.yurimontero.dagger.vo.User
import javax.inject.Inject

class UserViewModel @Inject constructor(private val userRepository: UserRepository ) : ViewModel() {

    fun getUsers() = userRepository.getUsers()

    fun insertUser(user: User) {
        userRepository.insertUser(user)
    }

    fun deleteUser(user: User) {
        userRepository.delete(user)
    }

    fun getOff() = userRepository.getOof()

    fun insertOof(oof: OOF) {
        userRepository.insertOof(oof)
    }

}
