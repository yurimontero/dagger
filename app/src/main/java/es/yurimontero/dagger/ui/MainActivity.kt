package es.yurimontero.dagger.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.view.Menu
import android.view.MenuItem
import android.view.View
import dagger.android.AndroidInjection
import es.yurimontero.dagger.R
import es.yurimontero.dagger.utils.inflate
import es.yurimontero.dagger.utils.log
import es.yurimontero.dagger.viewmodel.UserViewModel
import es.yurimontero.dagger.vo.OOF
import es.yurimontero.dagger.vo.Product
import es.yurimontero.dagger.vo.User
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_user.view.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(), View.OnClickListener{

    @Inject
    lateinit var userViewModel: UserViewModel

    @Inject
    lateinit var userAdapter: UserListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        userAdapter.setCallback {
            showChooserDialog(it)
        }

        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recyclerView.adapter = userAdapter

        userViewModel.getUsers().observe(this, Observer { users ->
            users?.also {
                userAdapter.submitList(it.data)
            }
        })

        floatingActionButton.setOnClickListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items
        when (item.itemId) {
            R.id.action_add -> {
                userViewModel.insertOof(OOF(0, "Leche", Product("1234567890123", "Alimentación")))
                return true
            }
            R.id.action_log -> {
                userViewModel.getOff().observe(this, Observer { values ->
                    values?.forEach {
                        log(it.toString())
                    }
                })
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View) {

        when (v) {
            floatingActionButton -> showEditDialog(User(), true)
        }
    }

    private fun showEditDialog(user: User, editable: Boolean) {

        val view = inflate(R.layout.dialog_user)

        view.etFirstName.setText(user.firstName)
        view.etLastName.setText(user.lastName)

        view.etFirstName.isEnabled = editable
        view.etLastName.isEnabled = editable

        AlertDialog
            .Builder(this)
            .setTitle(
                if (editable) {
                    if (user.id == 0) {
                        "New"
                    } else {
                        "Edit"
                    }
                } else {
                    "View"
                }
            )
            .setView(view)
            .setPositiveButton("Ok") { _, _ ->
                if (editable)
                    if (!view.etFirstName.text.toString().trim().isEmpty()) {

                        val tempUser = User(
                            user.id,
                            view.etFirstName.text.toString().trim(),
                            view.etLastName.text.toString().trim()
                        )
                        log(tempUser.toString())
                        userViewModel.insertUser(tempUser)
                    }
            }
            .show()
    }

    private fun showChooserDialog(user: User) {
        val colors = arrayOf("Ver", "Editar", "Eliminar")

        AlertDialog.Builder(this)
            // .setTitle("Pick a color")
            .setItems(colors) { _, which ->
                when (which) {
                    0 -> showEditDialog(user, false)
                    1 -> showEditDialog(user, true)
                    2 -> userViewModel.deleteUser(user)
                }
            }
            .show()
    }

}
