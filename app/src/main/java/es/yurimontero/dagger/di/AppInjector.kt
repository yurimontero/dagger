package es.yurimontero.dagger.di

import es.yurimontero.dagger.App

object AppInjector {

    fun init(app: App) {

        DaggerAppComponent.builder().application(app).build().inject(app)

    }

}
