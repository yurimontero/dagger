package es.yurimontero.dagger.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import es.yurimontero.dagger.ui.MainActivity

@Suppress("unused")
@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

}
