package es.yurimontero.dagger.vo

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class User(@PrimaryKey(autoGenerate = true) val id: Int, @SerializedName("username") val firstName: String,@SerializedName("name") val lastName: String) {
    @Ignore
    constructor() : this(0, "", "")
}
