package es.yurimontero.dagger.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import es.yurimontero.dagger.vo.OOF
import es.yurimontero.dagger.vo.User

@Database(
        entities = [User::class, OOF::class],
        version = 1,
        exportSchema = false
)
@TypeConverters(Converters::class)
abstract class DB : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun oofDao(): OofDao

}
