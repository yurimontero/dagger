package es.yurimontero.dagger.repository

import android.arch.lifecycle.LiveData
import es.yurimontero.dagger.api.JsonPlaceholderService
import es.yurimontero.dagger.db.OofDao
import es.yurimontero.dagger.db.UserDao
import es.yurimontero.dagger.utils.AppExecutors
<<<<<<< HEAD
=======
import es.yurimontero.dagger.vo.OOF
>>>>>>> b55aa8096f6e1af2784c9fd2ad04a6963b5df7d3
import es.yurimontero.dagger.vo.Resource
import es.yurimontero.dagger.vo.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
        private val appExecutors: AppExecutors,
        private val userDao: UserDao,
        private val oofDao: OofDao,
        private val jsonPlaceholderService: JsonPlaceholderService
) {

    fun getUsers(): LiveData<Resource<List<User>>> {
        return object : NetworkBoundResource<List<User>, List<User>>(appExecutors) {

            override fun saveCallResult(item: List<User>) {
                userDao.insert(item)
            }

            override fun shouldFetch(data: List<User>?): Boolean {
                return data == null || data.isEmpty() //|| repoListRateLimit.shouldFetch()
            }

            override fun loadFromDb() = userDao.getUsers()

            override fun createCall() = jsonPlaceholderService.getUsers()

        }.asLiveData()
    }

    fun insertUser(user: User) {
        appExecutors.diskIO().execute {
            userDao.insert(user)
        }
    }

    fun delete(user: User) {
        appExecutors.diskIO().execute {
            userDao.delete(user)
        }
    }

    fun getOof() = oofDao.getOof()


    fun insertOof(oof: OOF) {
        appExecutors.diskIO().execute {
            oofDao.insert(oof)
        }
    }

}
